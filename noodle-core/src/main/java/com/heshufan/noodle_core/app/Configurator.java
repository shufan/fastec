package com.heshufan.noodle_core.app;

import android.graphics.Bitmap;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconFontDescriptor;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * 配置类
 *
 * @author heshufan
 * @date 2018/11/9
 */

public class Configurator {

    /**
     * 存储全局的配置信息
     */
    private static final HashMap<String, Object> NOODLE_CONFIGS = new HashMap<>();

    private static ArrayList<IconFontDescriptor> ICON_FONT_DESCRIPTORS = new ArrayList();


    private Configurator() {
        NOODLE_CONFIGS.put(ConfigType.CONFIG_READY.name(), false);
    }

    /**
     * 静态内部类的单例模式
     */
    private static final class Holder {
        private static final Configurator INSATANCE = new Configurator();
    }

    public static Configurator getInstance() {
        return Holder.INSATANCE;
    }

    public HashMap<String, Object> getNoodleConfigs() {
        return NOODLE_CONFIGS;
    }

    /**
     * 配置完成
     */
    public final void configure() {
        initIcon();
        NOODLE_CONFIGS.put(ConfigType.CONFIG_READY.name(), true);
    }

    /**
     * 配置API_HOST
     *
     * @param host
     * @return
     */
    public Configurator withApiHost(String host) {
        NOODLE_CONFIGS.put(ConfigType.API_HOST.name(), host);
        return this;
    }

    /**
     * 检测是否初始化完成
     */
    private void checkConfiguration() {
        final boolean isReady = (boolean) NOODLE_CONFIGS.get(ConfigType.CONFIG_READY.name());
        if (!isReady) {
            throw new RuntimeException("Configuration is not ready, call configure");
        }
    }

    /**
     * 获得各项配置
     *
     * @param key 配置名
     * @param <T> 泛型
     * @return
     */
    final <T> T getConfiguration(Enum<ConfigType> key) {
        checkConfiguration();
        return (T) NOODLE_CONFIGS.get(key.name());

    }

    /**
     * 外部代理
     * @param descriptor
     * @return
     */
    public Configurator whitIcon(IconFontDescriptor descriptor) {
        ICON_FONT_DESCRIPTORS.add(descriptor);
        return this;

    }

    /**
     * 初始化Iconify
     */
    private void initIcon() {
        if (ICON_FONT_DESCRIPTORS.size() > 0) {
            Iconify.IconifyInitializer iconifyInitializer = Iconify.with(ICON_FONT_DESCRIPTORS.get(0));
            for (int i = 0; i < ICON_FONT_DESCRIPTORS.size(); i++) {
                iconifyInitializer.with(ICON_FONT_DESCRIPTORS.get(i));
            }
        }
    }
}
