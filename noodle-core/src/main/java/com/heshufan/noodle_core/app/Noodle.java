package com.heshufan.noodle_core.app;

import android.content.Context;

import java.util.HashMap;
import java.util.WeakHashMap;

/**
 *
 * Configure的代理类
 * @author heshufan
 * @date 2018/11/9
 */

public final class Noodle {

    /**
     * 初始化配置
     * @param context
     * @return
     */
    public static Configurator init(Context context) {
        getConfiguration().put(ConfigType.APPLICATION_CONTEXT.name(), context.getApplicationContext());
        return Configurator.getInstance();
    }

    /**
     * 获得config
     * @return
     */
    private static HashMap<String, Object> getConfiguration() {
        return Configurator.getInstance().getNoodleConfigs();
    }
}
