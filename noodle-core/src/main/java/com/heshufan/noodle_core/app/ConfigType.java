package com.heshufan.noodle_core.app;

/**
 * @author heshufan
 * @date 2018/11/9
 */

public enum ConfigType {
    // baseURL
    API_HOST,

    // 全局context
    APPLICATION_CONTEXT,

    // 是否配置完全
    CONFIG_READY,

    ICON
}
