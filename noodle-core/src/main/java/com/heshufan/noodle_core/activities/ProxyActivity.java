package com.heshufan.noodle_core.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.ContentFrameLayout;

import com.heshufan.noodle_core.R;
import com.heshufan.noodle_core.delegates.NoodleDelegate;

import me.yokeyword.fragmentation.SupportActivity;

/**
 * 各种fragment的容器,设置为抽象类，让使用者进行继承
 *
 * @author heshufan
 * @date 2018/11/9
 */

public abstract class ProxyActivity extends SupportActivity {

    /**
     * 设置rootDelegate，强制子类去实现
     * @return
     */
    public abstract NoodleDelegate setRootDeleagate();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initContainer(savedInstanceState);
    }

    private void initContainer(@Nullable Bundle savedInstanceState){
        //fragment的容器
        final ContentFrameLayout container = new ContentFrameLayout(this);
        //利用代码动态添加view,为view其设置id
        container.setId(R.id.delegate_container);
        setContentView(container);
        if (savedInstanceState == null) {
            loadRootFragment(R.id.delegate_container, setRootDeleagate());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //告诉垃圾收集器进行垃圾回收，但是不一定会调用
        System.gc();
        //强制调用已经失去引用的对象的finalize方法（finalize是对象被回收后的最后一个调用的方法）
        System.runFinalization();
    }
}
