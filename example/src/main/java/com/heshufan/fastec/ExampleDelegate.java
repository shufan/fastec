package com.heshufan.fastec;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.heshufan.noodle_core.delegates.NoodleDelegate;

/**
 * 项目的BaseFragment
 *
 * @author heshufan
 * @date 2018/11/12
 */

public class ExampleDelegate extends NoodleDelegate{
    @Override
    public Object setLayout() {
        return R.layout.example_delegate;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState, View view) {

    }
}
