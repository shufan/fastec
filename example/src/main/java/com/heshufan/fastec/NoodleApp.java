package com.heshufan.fastec;

import android.app.Application;

import com.heshufan.noodle_core.app.Noodle;
import com.heshufan.noodle_ec.icon.FontECModule;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

/**
 * @author heshufan
 * @date 2018/11/9
 */

public class NoodleApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Noodle.init(this)
                .whitIcon(new FontAwesomeModule())
                .whitIcon(new FontECModule())
                .withApiHost("http://127.0.0.1")
                .configure();
    }
}
