package com.heshufan.fastec;

import com.heshufan.noodle_core.activities.ProxyActivity;
import com.heshufan.noodle_core.delegates.NoodleDelegate;


/**
 * 项目的主activity
 *
 * @author heshufan
 */

public class ExampleActivity extends ProxyActivity {

    @Override
    public NoodleDelegate setRootDeleagate() {
        return new ExampleDelegate();
    }
}
