package com.heshufan.noodle_ec.icon;

import com.joanzapata.iconify.Icon;

/**
 * 引用第三方字体图标
 * 引用Alibaba矢量图标
 *
 * @author heshufan
 * @date 2018/11/9
 */

public enum FontECIcons implements Icon {

    /** 笔 */
    fe_pan('\uf606');

    char aChar;

    FontECIcons(char aChar) {
        this.aChar = aChar;
    }

    @Override
    public String key() {
        return name().replace('_', '-');
    }

    @Override
    public char character() {
        return aChar;
    }
}
