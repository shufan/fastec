package com.heshufan.noodle_ec.icon;

import com.joanzapata.iconify.Icon;
import com.joanzapata.iconify.IconFontDescriptor;

/**
 * 引用第三方字体图标
 *
 * @author heshufan
 * @date 2018/11/9
 */

public class FontECModule implements IconFontDescriptor {

    @Override
    public String ttfFileName() {
        return "iconfont.ttf";
    }

    @Override
    public Icon[] characters() {
        return FontECIcons.values();
    }
}
